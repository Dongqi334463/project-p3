<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws1</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    
      <div class="cotainer" style="text-align: center;">
      <h1>Verdachte moordzaak Kaatsheuvel in hoger beroep alsnog vrijgesproken</h1>
      <img src="./img/nieuws1.jpg" alt="nieuws1" height="300px">
      <p>De 46-jarige Wim S. is in hoger beroep vrijgesproken van de moord op zijn <br>
          toenmalige vriendin Heidy Goedhart in 2010. Het Openbaar Ministerie had <br>
          tegen S. een celstraf van 17,5 jaar geëist. Maar het hof in Den Haag oordeelde <br>
          dat S. mogelijk onder druk van undercoveragenten is verleid tot het doen van <br>
          een bekentenis.</p>
          <p>In december 2010 werd de 34-jarige partner van S. in
               de tuin van hun woning in <br>
               Kaatsheuvel met een steen op het hoofd geslagen en gewurgd. De kinderen <br>
               van het stel lagen op dat moment in bed te slapen. Het Openbaar Ministerie <br>
               wees S. aan als schuldige en beschuldigde hem ervan dat hij het misdrijf wilde <br>
               verhullen door een inbraak in scène te zetten.</p>
    </div>
</body>
</html>