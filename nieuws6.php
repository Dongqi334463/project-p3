<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    
      <div class="cotainer" style="text-align: center;">
        <h1>Opnieuw anonieme graven gevonden bij Canadese internaten</h1>
        <img src="./img/nieuws6.jpg" alt="" height="300px">
        <p>In Canada zijn opnieuw 54 graven gevonden bij twee voormalige katholieke <br>
           internaten. Het zijn waarschijnlijk graven van inheemse kinderen die tot het <br>
           einde van de vorige eeuw in internaten werden gestopt, om hen te dwingen zich <br>
           aan te passen aan de witte Canadese cultuur.</p>

         <p> De graven zijn gevonden door de Keeseekoose First Nation, een gemeenschap <br>
          van oorspronkelijke bewoners van Canada. "Ze zijn van de aardbodem <br>
          verdwenen, en nooit meer gezien", zegt de leider van de gemeenschap, Lee <br>
          Kitchemonia, tegen Canadese media. "Dat is het moeilijkste gedeelte."</p>
      </div>
</body>
</html>