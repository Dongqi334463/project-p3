<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    
      <div class="cotainer" style="text-align: center;">
         <h1>Excessen of structureel geweld? Onderzoek naar dekolonisatie Indonesië komt morgen</h1>
         <img src="./img/nieuws4.jpg" alt="" height="300px">
         <p>Na jaren onderzoek verschijnt morgen het rapport Onafhankelijkheid, <br>
             dekolonisatie, geweld en oorlog in Indonesië 1945-1950. Dit onderzoek, <br>
              uitgevoerd door het Koninklijk Instituut voor Taal-, Land- en Volkenkunde <br>
              (KITLV), het Nederlands Instituut voor Militaire Historie (NIMH) en het NIOD  <br>
              Instituut voor Oorlogs-, Holocaust- en Genocidestudies, kijkt onder meer naar <br>
               het Nederlandse geweld bij de dekolonisatie van Nederlands-Indië.</p>
               <p>Het onderzoek maakt voor publicatie al veel los. Een aantal vragen over de <br>
                   studie beantwoord.</p>
      </div>
</body>
</html>