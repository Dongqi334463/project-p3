<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

      <div class="cotainer" style="text-align: center;">
        <h1>Vlag uit in Oekraïne, ook voorzichtig optimisme bij de NAVO</h1>
        <img src="./img/nieuws9.jpg" alt="" height="300px">
        <p>De Oekraïense president Zelensky heeft vandaag uitgeroepen tot "dag van <br>
            eenheid". Hij vroeg de bevolking om vanaf 09.00 uur de nationale vlag uit te <br>
            hangen en het volkslied te zingen of te spelen. Hij deed dit op de dag waarop <br>
            volgens Amerikaanse inlichtingen de Russische invasie zou beginnen.</p>
        <p>Westerse landen houden nog steeds rekening met een invasie. De <br>
            Amerikaanse president Biden zei gisteren in een televisietoespraak dat een <br>
            invasie nog altijd mogelijk is. Hij benadrukte dat de Amerikanen nog niet zien <br>
            dat de Russen hun enorme troepenmacht aan de Oekraïense grens afbouwen.</p>
      </div>
</body>
</html>

