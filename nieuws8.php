<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
   
      <div class="cotainer" style="text-align: center;">
        <h1>Eekhoorns en boommarters gaan eindelijk over dure brug</h1>
        <img src="./img/nieuws8.jpg" alt="" height="300px">
        <p>Een faunabrug in Den Haag over de N44 wordt eindelijk gebruikt door <br>
            eekhoorns en boommarters. De brug werd in 2012 aangelegd om deze dieren <br>
            te laten oversteken. Wildcamera's registreerden aanvankelijk nauwelijks klein <br>
            overstekend wild, maar de dieren hebben de faunabrug dus toch omarmd.</p>
        <p>In 2021 staken de dieren ruim vierhonderd keer de Benoordenhoutseweg (de <br>
            N44) over tussen het Haagse Bos en landgoed Oosterbeek/Clingendael. "We <br>
            kunnen wel spreken van een groot succes", zegt stadsecoloog Esther Vogelaar <br>
            tegen Omroep West.</p>
      </div>
</body>
</html>