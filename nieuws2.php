<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
   
    <div class="cotainer" style="text-align: center;">
        <h1>Werkgevers op vingers getikt om afspraken over werving personeel</h1>
        <img src="./img/nieuws2.jpg" alt="" height="300px">
        <p>Personeel is tegenwoordig een schaars goed en voor veel bedrijven en <br>
            organisaties is de werving een steeds nijpender probleem. Het is niet alleen <br>
             lastig om mensen te krijgen, maar ook moeilijk personeel vast te houden. Om <br>

              wegkapen van elkaars personeel te voorkomen maken sommige werkgevers en <br>

              werkgeversorganisaties afspraken over het niet werven of in dienst nemen van <br>
               elkaars personeel.</p>

               <p>Maar werkgevers mogen dit soort afspraken helemaal niet maken, omdat <br>
                   mededingingsregels dit verbieden, zo waarschuwt de Autoriteit Consument en <br>
                   Markt (ACM). Werknemers hebben de vrijheid om te werken bij wie ze willen.</p>
    </div>
</body>
</html>