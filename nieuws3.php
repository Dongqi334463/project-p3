<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws3</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    
      <div class="cotainer" style="text-align: center;">
          <h1>Overheid wilde pas informeren over corona na 'grote klap'</h1>
          <img src="./img/nieuws3.jpg" alt="" height="300px">
          <p>Pas alarm slaan als het écht nodig is. Dat was de lijn van de overheid in de <br>
              eerste maanden van de coronacrisis, concludeert de Onderzoeksraad voor <br>
              Veiligheid (OVV) in het vandaag verschenen rapport.</p>

              <p>"Zolang de grote klap er nog niet was, zag de Rijksoverheid geen reden om <br>
                  actief naar buiten te treden om de bevolking van uitgebreide informatie over de <br>
                  crisis te voorzien", schrijft de OVV in het ruim 300 pagina's dikke document. <br>
                  "Dat resulteerde in geruststellende communicatie door het RIVM in de periode <br>
                  dat het virus nog niet in Nederland was vastgesteld."</p>
      </div>
</body>
</html>