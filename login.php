<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Login</title>
</head>
<body>

    <h2>Login</h2>

    <form action="./index.php?content=login_script" method="post">
      <div class="imgcontainer">
        <img src="img/header.jpg" alt="Avatar" class="avatar">
      </div>

      <div class="container">
        <label for="email"><b>Vul hier uw e-mailadres in:</b></label>
        <input type="text" placeholder="Enter E-mailadres" name="email" id="inputEmail" required autofocus>

        <label for="psw"><b>Vul hier uw wachtwoord:</b></label>
        <input type="password" placeholder="Enter Wachtwoord" name="password" id="inputPassword" required>

        <button type="submit">Login</button>
        <label>
          <input type="checkbox" checked="checked" name="remember"> Remember me
        </label>
      </div>

      <div class="container" style="background-color:#f1f1f1">
       
        <span class="psw">Forgot <a href="#">password?</a></span>

         <p>Don't have an account?<a href="index.php?content=register">Register</a>.</p>
       
      </div>
    </form>






</body>
</html>