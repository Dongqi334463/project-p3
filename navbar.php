<?php
  $active = (isset($_GET["content"]))? $_GET["content"]: "";
?>

<div class="header">
        <img class="logo" src="img/logo.png" alt="logo" >
        <div class="header-right">
          <a  href="./index.php?content=home" <?php echo (in_array($active, ["home", ""]))? "active": "" ?> style="font-size: xx-large;" ><strong>Home</strong></a>
        <?php
        if(isset($_SESSION["id"])) {
          switch( $_SESSION["userrole"] ) {
              case 'customer':
                echo '<a href="./index.php?content=overzicht"'; echo ($active == "overzicht" )? "active": ""; echo '> Overzicht</a>
                <a href="./index.php?content=c-home"'; echo ($active == "c-home" )? "active": ""; echo '> Mijn gegevens</a>';
              break;
              case 'moderator':
                echo '<a href="./index.php?content=overzicht"'; echo ($active == "overzicht" )? "active": ""; echo' > Overzicht</a>
                 <a href="./index.php?content=persberichten"'; echo ($active == "persberichten" )? "active": ""; echo'>Nieuwsplaatsen</a>
                 <a href="./index.php?content=read"'; echo ($active == "read" )? "active": ""; echo'>Read</a>
                 <a href="./index.php?content=m-home"'; echo ($active == "m-home" )? "active": ""; echo '> Mijn gegevens</a>';
              break;
              case 'admin':
                echo '<a href="./index.php?content=overzicht"'; echo ($active == "overzicht" )? "active": ""; echo '> Overzicht</a>
                <a href="./index.php?content=accountpage"'; echo ($active == "accountpage" )? "active": ""; echo '> Accountpage</a>
                <a href="./index.php?content=read"'; echo ($active == "read" )? "active": ""; echo '> Read</a>
                <a href="./index.php?content=a-home"'; echo ($active == "a-home" )? "active": ""; echo '> Mijn gegevens</a>';
              break;
              default:
              break;
          } 
          echo '<a class="active" href="./index.php?content=logout"'; echo ($active == "logout" )? "active": ""; echo ' style="font-size: x-large;" >logout</a>';
        } else {
           echo '<a href="./index.php?content=register"'; echo ($active == "register" )? "active": ""; echo '>Registratie</a>
           <a class="active" href="./index.php?content=login"'; echo ($active == "login" )? "active": ""; echo 'style="font-size: x-large;" >Login</a>';
        }
        ?>
        </div>
</div>
