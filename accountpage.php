<?php
include("./connect_db.php");

$sql = "SELECT * FROM `register` ";   

$result = mysqli_query($conn, $sql);

$row = ""; 
while ($record = mysqli_fetch_assoc($result)) {
    $row .= "<tr>                     
                <td>{$record['id']}</td>
                <td>{$record['naam']}</td>
                <td>{$record['tussenvoegsel']}</td>
                <td>{$record['achternaam']}</td>
                <td>{$record['email']}</td>
                <td>{$record['password']}</td>
                <td>{$record['userrole']}</td>
                <td>{$record['activated']}</td>
                <td>
                <a href= './delete.php? id={$record['id']}'> 
                    <span>&#10006;</span>
                </a>
                </td>
            </tr>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
             <!-- de tabel met gegevens uit de databse komt hier -->

                <table class="table table-striped table-info table-hover">
                    <thead>
                        <tr class="table-warning">                            
                            <th>id</th>
                            <th>naam</th>
                            <th>tussenvoegsel</th>
                            <th>achternaam</th>
                            <th>email</th>
                            <th>password</th>
                            <th>userrole</th>
                            <th>activated</th>
                            <th>&nbsp;</th>
                        </tr>     
                    </thead>
                    <tbody>                     
                    <?php echo $row; ?>
                    </tbody>
                </table>
            </div>
        </div>
</body>
</html>