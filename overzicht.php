<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Overzicht</title>
    <link rel="stylesheet" href="./css/style.css">

    
</head>
<body>
   

      <div class="row">
          <div class="leftcolumn">
              <div class="card">
              <h1>Overzicht</h1>
              Select datum: <input type="date" name="selectdate" id="selectdate">
               <br>
              <p><a href="./index.php?content=nieuws" style="color: blue; text-decoration: none;">16/02/22: 'Zonder booster twee keer zoveel ziekenhuisopnames' • Aantal besmettingen wereldwijd daalt</a></p>
              <p><a href="./index.php?content=nieuws1" style="color: blue; text-decoration: none;">16/02/22: Verdachte moordzaak Kaatsheuvel in hoger beroep alsnog vrijgesproken</a></p>
              <p><a href="./index.php?content=nieuws2" style="color: blue; text-decoration: none;">16/02/22: Werkgevers op vingers getikt om afspraken over werving personeel</a></p>
              <p><a href="./index.php?content=nieuws3" style="color: blue; text-decoration: none;">16/02/22: Overheid wilde pas informeren over corona na 'grote klap'</a></p>
              <p><a href="./index.php?content=nieuws4" style="color: blue; text-decoration: none;">16/02/22: Excessen of structureel geweld? Onderzoek naar dekolonisatie Indonesië komt morgen</a></p>
              <p><a href="./index.php?content=nieuws5" style="color: blue; text-decoration: none;">16/02/22: Van Dissel optimistisch: geen grote golf in ziekenhuizen meer door omikron</a></p>
              <p><a href="./index.php?content=nieuws6" style="color: blue; text-decoration: none;">16/02/22: Opnieuw anonieme graven gevonden bij Canadese internaten</a></p>
              <p><a href="./index.php?content=nieuws7" style="color: blue; text-decoration: none;">16/02/22: OM wil 10 jaar celstraf voor man die Els Slurink doodde</a></p>
              <p><a href="./index.php?content=nieuws8" style="color: blue; text-decoration: none;">16/02/22: Eekhoorns en boommarters gaan eindelijk over dure brug</a></p>
              <p><a href="./index.php?content=nieuws9" style="color: blue; text-decoration: none;">16/02/22: Vlag uit in Oekraïne, ook voorzichtig optimisme bij de NAVO</a></p>
              </div>
              
          </div>
          <div class="rightcolumn">
              <div class="card">
                <a href="./index.php?content=nieuws" ><h4>16/02/22: 'Zonder booster twee keer zoveel ziekenhuisopnames' • Aantal besmettingen wereldwijd daalt</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws2"><h4>16/02/22: Werkgevers op vingers getikt om afspraken over werving personeel</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws4"><h4>16/02/22: Excessen of structureel geweld? Onderzoek naar dekolonisatie Indonesië komt morgen</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws6"><h4>16/02/22: Opnieuw anonieme graven gevonden bij Canadese internaten</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws8"><h4>16/02/22: Eekhoorns en boommarters gaan eindelijk over dure brug</h4></a>
              </div>
          </div>
          <div class="column">
              <div class="card">
                <a href="./index.php?content=nieuws1"><h4>16/02/22: Verdachte moordzaak Kaatsheuvel in hoger beroep alsnog vrijgesproken</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws3"><h4>16/02/22: Overheid wilde pas informeren over corona na 'grote klap'</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws5"><h4>16/02/22: Van Dissel optimistisch: geen grote golf in ziekenhuizen meer door omikron</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws7"><h4>16/02/22: OM wil 10 jaar celstraf voor man die Els Slurink doodde</h4></a>
              </div>
              <div class="card">
                <a href="./index.php?content=nieuws9"><h4>16/02/22: Vlag uit in Oekraïne, ook voorzichtig optimisme bij de NAVO</h4></a>
              </div>
          </div>
      </div>
</body>
</html>