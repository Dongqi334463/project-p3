<h1 class="title1">Dagelijks Papier</h1>

<img class="photo1" src="./img/breaking-news.gif" alt="photo1">
<h4 class="title2">Welkom Beste Nieuwslezers</h4>
<p class="text1">Op onze website leest u last minute actueel nieuws online.
  Eerst moet u zich registreren, daarna kunt u inloggen en het actuele nieuws lezen dat onze redactie voor u heeft geschreven.
  U kunt onze website gemakkelijk openen vanaf uw telefoon, computer en tablet en het nieuws lezen.Het is compatibel met alle technologische apparaten.</p>

<article class="all-nieuws">
  <h1>Het Laatste Nieuws</h1>
  <article class="nieuw">
    <a href="./index.php?content=nieuws1">
      <h2><img src="./img/mord.jpg" alt="corona" style="width: 20%;"></h2>
    </a>
    <a href="./index.php?content=nieuws1">Verdachte moordzaak Kaatsheuvel in hoger beroep alsnog vrijgesproken</a>
  </article>
  <article class="nieuw">
    <a href="./index.php?content=nieuws2">
      <h2><img src="./img/werk.jpg" alt="werk" style="width: 20%;"></h2>
    </a>
    <a href="./index.php?content=nieuws2">Werkgevers op vingers getikt om afspraken over werving personeel</a>
  </article>
  <article class="nieuw">
    <a href="./index.php?content=nieuws3">
      <h2><img src="./img/pers.jpg" alt="pers" style="width: 20%;"></h2>
    </a>
    <a href="./index.php?content=nieuws3">Overheid wilde pas informeren over corona na 'grote klap'</a>
  </article>
</article>