<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Dagelijks Papier</title>
</head>
<body>

    <main>
     <section class="container-fluid px-0">
        <div class="row">
          <div class="col-12">
            <?php include("./banner.php"); ?>
          </div>
        </div>
      </section>
     <section class="container-fluid px-0">
        <div class="row">
          <div class="col-12">
            <?php include("./navbar.php"); ?>
          </div>
        </div>
      </section>
      <section class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php include("./content.php"); ?>
          </div>
        </div>
      </section>
      <section class="container-fluid px-0 fixed-bottom">
        <div class="row">
          <div class="col-12">
            <?php include("./footer.php"); ?>
          </div>
        </div>
      </section>
    </main>

</body>
</html>