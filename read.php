<?php
include("./connect_db.php");

$sql = "SELECT * FROM `nieusplaatsen` ";   

$result = mysqli_query($conn, $sql);

$row = ""; 
while ($record = mysqli_fetch_assoc($result)) {
    $row .= "<tr>                     
                <td>{$record['titel']}</td>
                <td>{$record['datum']}</td>
                <td>{$record['afbeelding']}</td>
                <td>{$record['text']}</td>
                <td>{$record['naam']}</td>
                <td>{$record['plaats']}</td>
                <td>{$record['email']}</td>
                <td>
                  <a href= './update.php?titel={$record['titel']}'> 
                  <span>&#9784;</span>
                  </a>
                </td>
            </tr>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
             <!-- de tabel met gegevens uit de databse komt hier -->

                <table class="table table-striped table-info table-hover">
                    <thead>
                        <tr class="table-warning">                            
                            <th>titel</th>
                            <th>datum</th>
                            <th>afbeelding</th>
                            <th>text</th>
                            <th>naam</th>
                            <th>plaats</th>
                            <th>email</th>
                            <th>&nbsp;</th>
                        </tr>     
                    </thead>
                    <tbody>                     
                    <?php echo $row; ?>
                    </tbody>
                </table>
            </div>
        </div>
</body>
</html>