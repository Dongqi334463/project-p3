<?php
 if (!(isset($_GET["content"]) && isset($_GET["id"]) && isset($_GET["pwh"]))){
  header("Location: ./index.php?content=home");
 }
?>

<div class="container">
    <div class="row">
        <div class="col-12 col-sm-6">
            <form action="./index.php?content=activate_script" method="post">
              
                <label for="inputPassword" class="form-label" >Kies een nieuwe wachtwoord:</label>
                <input name="password" type="password" class="form-control" id="inputPassword" aria-describedby="passwordHelp" placeholder="Kies een veilig wachtwoord..." autofocus>
                <div id="passwordHelp" class="form-text" > </div>
              </div>   
                 
              <label for="inputPasswordCheck">Type het wachtwoord opnieuw:</label>
                <input name="passwordCheck" type="password" class="form-control" id="inputPassword" aria-describedby="passwordHelpCheck" placeholder="Ter controle voert u nogmaals uw wachtwoord in...">
                <div id="passwordHelpCheck" class="form-text"></div>
              </div>

              <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
              <input type="hidden" name="pwh" value="<?php echo $_GET["pwh"]; ?>">
 
              <div><button type="submit" style="background-color: #aa4f04;
    color: white;
    padding: 5px 10px;
    margin: 4px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
    text-align: center;">Activeer</button > </div>     

            </form>
        </div>
    </div>    
</div>