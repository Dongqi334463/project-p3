<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws1</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
       
    <div class="cotainer" style="text-align: center;">
        <h1>63.779 positieve tests • 'Zonder booster twee keer zoveel ziekenhuisopnames'</h1>
        <p> <h4> 1.Welkom in het coronablog van woensdag 16 februari.</p></h4>
          <p> <h4>2.Vanaf vandaag minder landen met 'oranje' reisadvies.</p> </h4>
          <p> <h4> 3.Onderzoeksraad: Nederland was onvoldoende voorbereid op pandemie.</p></h4>
         <p>  <h4> 4.WHO: aantal besmettingen wereldwijd daalt met 19 procent.</p></h4>
          <h2>63.779 positieve tests, ziekenhuisbezetting neemt iets af</h2>
         <p>Bij het RIVM zijn tot 10.00 uur vanmorgen 63.779 nieuwe positieve tests <br> Dat zijn er ruim 10.000 meer dan gisteren (53.522).<br> Verder werden er bij het RIVM 18 overleden covid-patiënten gemeld. <br> De afgelopen zeven dagen registreerde het RIVM gemiddeld 13 doden per dag, tegen 7 <br>doden per dag een week eerder.</p>
         
         <p>In de ziekenhuizen zijn 179 nieuwe coronapatiënten opgenomen op de verpleegafdeling.<br> Op de IC's kwamen er 12 nieuwe patiënten bij.</p>
         
         <p>In de ziekenhuizen nam de bezetting iets af. <br>Er liggen nu 1650 mensen met corona in het ziekenhuis (gisteren: 1656),<br> van wie 173 op de IC (gisteren: 181),<br> meldt het Landelijk Coördinatiecentrum Patiënten Spreiding (LCPS).</p>
             
    </div>
</body>
</html>