<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
   
      <div class="cotainer" style="text-align: center;">
        <h1>OM wil 10 jaar celstraf voor man die Els Slurink doodde</h1>
        <img src="./img/nieuws7.jpg" alt="" height="300px">
        <p>Als het aan het Openbaar Ministerie ligt, gaat Jahangir A. 10 jaar de gevangenis <br>
          in. Het OM acht de 45-jarige man uit Geldrop schuldig aan de moord op de 33- <br>
          jarige psychologe Els Slurink uit Groningen, een coldcasezaak uit 1997.</p>
        <p>De psychologe Els Slurink werd op 21 maart 1997 dood gevonden in haar <br>
          woning. Ze zou een werkafspraak hebben. Doordat ze daar niet verscheen <br>
          kregen collega's argwaan en gingen ze naar haar huis, schrijft RTV Noord.</p>
      </div>
</body>
</html>