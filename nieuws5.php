<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nieuws</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
   
      <div class="cotainer" style="text-align: center;">
            <h1>Van Dissel optimistisch: geen grote golf in ziekenhuizen meer door omikron</h1>
            <img src="./img/nieuws5.jpg" alt="" height="300px">
            <p>Het RIVM verwacht geen grote toename van het aantal patiënten in de  <br>
              ziekenhuizen meer door de omikronvariant. Jaap van Dissel, directeur van het <br>
              Centrum voor Infectieziektenbestrijding van het RIVM heeft dat gezegd bij de <br>
              coronabriefing van de Tweede Kamer.</p>

             <p> Het aantal ziekenhuisopnames zal waarschijnlijk nog oplopen, maar de piek <br>
               komt waarschijnlijk veel minder hoog uit dan werd gedacht. Zelfs met de <br>
               versoepelingen in het vooruitzicht komt de piek onder die van de deltavariant te <br>
               liggen, verwacht Van Dissel. Eerder nog hield het RIVM rekeningen met een <br>
               overbelasting van de ziekenhuizen.</p>
      </div>
</body>
</html>