<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Register</title>
</head>
<body>


    <form action="./index.php?content=register_script" method="post" >
        <div class="container">
            <div class= "card">
              <h1 class="registertext">Register</h1>
              <p>Vul dit formulier in om een account aan te maken.</p>
              <hr>
              
              <label for="naam">Vul hier uw naam in:</label>
              <input type="text" name="naam" id="naam" placeholder="Naam" >

              <label for="tussenvoegsel">Vul hier uw tussenvoegsel in:</label>
              <input type="text" name="tussenvoegsel" id="tussenvoegsel" placeholder="Tussenvoegsel" >

              <label for="achternaam">Vul hier uw achternaam in:</label>
              <input type="text" name="achternaam" id="achternaam" placeholder="Achternaam" >


              <label for="inputEmail">Vul hier uw e-mailadres in:</label>
              <input type="text" placeholder=" Email" name="email" id="email" required autofocus>
              
              
              <p>Door een account aan te maken gaat u akkoord met onze <a href="#">Algemene Voorwaarden & Privacy.</a>.</p>
              <button type="submit" class="registerbtn">Register</button>
            </div>
        </div>
        
        <div class="container signin">
            <p>Heb je al een account? <a href="index.php?content=login">Aanmelden.</a>.</p>
        </div>
    </form>
    
    
    
    <!-- 
        
        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="psw" id="psw" required>
        
        <label for="psw-repeat"><b>Repeat Password</b></label>
        <input type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat" required>
        <hr>
        
        
        
            -->



</body>
</html>